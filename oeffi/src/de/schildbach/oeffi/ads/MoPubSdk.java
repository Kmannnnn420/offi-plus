package de.schildbach.oeffi.ads;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.inmobi.sdk.InMobiSdk;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentDialogListener;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.AdColonyAdapterConfiguration;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.VungleAdapterConfiguration;
import com.zookey.universalpreferences.UniversalPreferences;

import net.media.android.base.pub.MNet;

import java.util.HashMap;
import java.util.Map;

import de.schildbach.oeffi.BuildConfig;
import de.schildbach.oeffi.FirebaseConfigurator;
import de.schildbach.oeffi.ads.inmobi.InMobiAdapterConfiguration;

/**
 * Created by Sönke Gissel on 16.12.2019.
 */
public class MoPubSdk {

    public enum AdType {
        BANNER,
        INTERSTITIAL,
        SDK;
    }

    private static MoPubSdk INSTANCE;
    private final MutableLiveData<Boolean> isMoPubSdkInitialized = new MutableLiveData<>();
    private Activity mActivity;

    private MoPubSdk(Activity activity) {
        this.mActivity = activity;

        Map<String, String> adColonySettings = new HashMap<>();
        adColonySettings.put("appId", "app13c535f1d54849ab82");
        adColonySettings.put("zoneId", "vzf40f4a55b6294f449e");
        adColonySettings.put("allZoneIds", "[\"vzf40f4a55b6294f449e, vzb1d8f5903d1d485793\"]");

        Map<String, String> vungleSettings = new HashMap<>();
        vungleSettings.put("appId", "5e4271bae0cc3f001753a96e");

        SdkConfiguration sdkConfiguration = new SdkConfiguration.Builder("362da1882dfc49f3a33b92ea1708f3a5")
                .withMediatedNetworkConfiguration(AdColonyAdapterConfiguration.class.getName(), adColonySettings)
                .withMediatedNetworkConfiguration(VungleAdapterConfiguration.class.getName(), vungleSettings)
                .withAdditionalNetwork(InMobiAdapterConfiguration.class.getName())
                .withLogLevel(BuildConfig.DEBUG ? MoPubLog.LogLevel.DEBUG : MoPubLog.LogLevel.NONE)
                .build();
        if(eligibleForAds(AdType.SDK)) {
            MoPub.initializeSdk(activity, sdkConfiguration, initSdkListener());

            MNet.init(mActivity.getApplication(), "8CUG6YG67");
            if(BuildConfig.DEBUG) {
                MNet.setTestMode(true, null);
                InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);
            }
        }

        if((int) UniversalPreferences.getInstance().get("app_starts", 0)
                == FirebaseConfigurator.getAppStartAds()) {
            FirebaseConfigurator.sendEvent(activity, "benutzer_sieht_jetzt_werbung");
        }
    }

    public static MoPubSdk getInstance(Activity activity) {
        if(MoPubSdk.INSTANCE == null) {
            MoPubSdk.INSTANCE = new MoPubSdk(activity);
        }
        return MoPubSdk.INSTANCE;
    }

    private SdkInitializationListener initSdkListener() {
        return new SdkInitializationListener() {
            @Override
            public void onInitializationFinished() {
           /* MoPub SDK initialized.
           Check if you should show the consent dialog here, and make your ad requests. */
                Log.d("MoPub", "SDK initialized");
                isMoPubSdkInitialized.setValue(true);
                showConsentIfNeeded();
            }
        };
    }

    private void showConsentIfNeeded() {
        PersonalInfoManager mPersonalInfoManager = MoPub.getPersonalInformationManager();

        Log.d("customeee", "Can collect pers information? "+MoPub.canCollectPersonalInformation()
                + ".\nShould show consent dialog? "+mPersonalInfoManager.shouldShowConsentDialog());

        if(!MoPub.canCollectPersonalInformation()) {
            if(mPersonalInfoManager.shouldShowConsentDialog()) {
                mPersonalInfoManager.loadConsentDialog(new ConsentDialogListener() {
                    @Override
                    public void onConsentDialogLoaded() {
                        mPersonalInfoManager.showConsentDialog();
                    }

                    @Override
                    public void onConsentDialogLoadFailed(@NonNull MoPubErrorCode moPubErrorCode) {
                        MoPubLog.i("Consent dialog failed to load.");
                    }
                });
            }
        }
    }

    public static boolean eligibleForAds(AdType adType) {
        Log.i("MoPubSDK", "Wirkliche Appstarts: "
                + UniversalPreferences.getInstance().get("app_starts", 0)
                + ". AppStarts nötig für Werbung: " + FirebaseConfigurator.getAppStartAds()
                +". "+ (adType == AdType.BANNER ? "Banner" : "Interstitial")+" Werbung angeschaltet? "+
                (adType == AdType.BANNER ? FirebaseConfigurator.showAdsFooter() : FirebaseConfigurator.showAdsInterstitial()));
        return (int) UniversalPreferences.getInstance().get("app_starts", 0)
                >= FirebaseConfigurator.getAppStartAds()
                && (adType == AdType.BANNER ? FirebaseConfigurator.showAdsFooter() : FirebaseConfigurator.showAdsInterstitial())
                || BuildConfig.DEBUG;
    }

    public LiveData<Boolean> isMoPubSdkInitialized() {
        return isMoPubSdkInitialized;
    }
}
