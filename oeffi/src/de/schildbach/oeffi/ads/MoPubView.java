package de.schildbach.oeffi.ads;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.mopub.mobileads.MoPubErrorCode;

import de.schildbach.oeffi.BuildConfig;
import de.schildbach.oeffi.R;


public class MoPubView {

    private com.mopub.mobileads.MoPubView moPubView;
    private Activity mActivity;

    public MoPubView(Activity activity) {
        this.mActivity = activity;
        moPubView = activity.findViewById(R.id.banner);
        moPubView.setAdUnitId(BuildConfig.DEBUG ? "b195f8dd8ded45fe847ad89ed1d016da" : "4af7a68bac7c4cc2b98c4a528bba21de");
        //moPubView.setAdUnitId("4af7a68bac7c4cc2b98c4a528bba21de");
        moPubView.setAdSize(getAdSize());

        moPubView.setKeywords("Navigation, Maps, Bus, Bahn, Flixbus, Netzpläne, Haltestellen");


        moPubView.setBannerAdListener(new com.mopub.mobileads.MoPubView.BannerAdListener() {
            @Override
            public void onBannerLoaded(com.mopub.mobileads.MoPubView banner) {
                moPubView.setVisibility(View.VISIBLE);
                Log.d("MoPubView", "onBannerLoaded.");
            }

            @Override
            public void onBannerFailed(com.mopub.mobileads.MoPubView banner, MoPubErrorCode errorCode) {
                moPubView.setVisibility(View.GONE);
                Log.d("MoPubView", "onBannerFailed. "+errorCode.name());
            }

            @Override
            public void onBannerClicked(com.mopub.mobileads.MoPubView banner) {

            }

            @Override
            public void onBannerExpanded(com.mopub.mobileads.MoPubView banner) {

            }

            @Override
            public void onBannerCollapsed(com.mopub.mobileads.MoPubView banner) {

            }
        });

        loadMoPubView();
    }

    private void loadMoPubView() {
        if(MoPubSdk.eligibleForAds(MoPubSdk.AdType.BANNER)) {
            moPubView.loadAd();
            Log.i("MoPubView", "Werbung wird gezeigt! Größe "+moPubView.getAdSize());
        }
        else {
            Log.i("MoPubView", "Werbung wird nicht gezeigt!");
        }
    }

    public com.mopub.mobileads.MoPubView getMoPubView() {
        return moPubView;
    }

    private com.mopub.mobileads.MoPubView.MoPubAdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        int orientation = Resources.getSystem().getConfiguration().orientation;
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float widthPixels;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            widthPixels = metrics.widthPixels;
        } else {
            widthPixels = metrics.heightPixels;
        }
        float density = metrics.density;
        int adWidth = (int) (widthPixels / density);
        Log.d("customeee", "widthPixels "+widthPixels+" density "+density+" adWidth "+adWidth);
        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        if(adWidth < 480) {
            return com.mopub.mobileads.MoPubView.MoPubAdSize.HEIGHT_50;
        } else {
            return com.mopub.mobileads.MoPubView.MoPubAdSize.HEIGHT_90;
        }
    }
}