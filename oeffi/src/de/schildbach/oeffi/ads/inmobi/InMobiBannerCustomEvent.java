package de.schildbach.oeffi.ads.inmobi;


import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.InMobiBanner;
import com.inmobi.ads.InMobiBanner.AnimationType;
import com.inmobi.ads.exceptions.SdkNotInitializedException;
import com.inmobi.ads.listeners.BannerAdEventListener;
import com.inmobi.sdk.InMobiSdk;
import com.inmobi.unification.sdk.InitializationStatus;
import com.mopub.common.DataKeys;
import com.mopub.common.MoPub;
import com.mopub.common.logging.MoPubLog;
import com.mopub.mobileads.CustomEventBanner;
import com.mopub.mobileads.MoPubErrorCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class InMobiBannerCustomEvent extends CustomEventBanner {


    private CustomEventBannerListener mBannerListener;
    private static boolean mIsInMobiSdkInitialized = false;
    private String accountId = "";
    private long placementId = -1;
    private static final String TAG = InMobiBannerCustomEvent.class.getSimpleName();
    private int adWidth = 0;
    private int adHeight = 0;

    private InMobiBanner imbanner;

    @Override
    protected void loadBanner(Context context, CustomEventBannerListener customEventBannerListener,
                              Map<String, Object> localExtras, Map<String, String> serverExtras) {
        mBannerListener = customEventBannerListener;
        InMobiSdk.setLogLevel(InMobiSdk.LogLevel.DEBUG);

        try {
            final JSONObject serverParams = new JSONObject(serverExtras);
            accountId = serverParams.getString("accountid");
            placementId = serverParams.getLong("placementid");


            final JSONObject localParmas = new JSONObject(localExtras);
            adWidth = localParmas.getInt(DataKeys.AD_WIDTH);
            adHeight = localParmas.getInt(DataKeys.AD_HEIGHT);
            MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, String.valueOf(placementId));
            MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, accountId);

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (!mIsInMobiSdkInitialized) {
            try {
                @InitializationStatus String status = InMobiSdk.init(context, accountId,
                        InMobiGDPR.getGDPRConsentDictionary());
                if (!InitializationStatus.SUCCESS.equals(status)) {
                    throw new Exception("SDK Initialize failed");
                }
                mIsInMobiSdkInitialized = true;
            } catch (Exception e) {
                Log.e(TAG, "Exception occured while Initializing InMobi SDK", e);
                handleBannerInitializationFailure();
                return;
            }
        } else {
            InMobiSdk.updateGDPRConsent(InMobiGDPR.getGDPRConsentDictionary());
        }
        /*
        Sample for setting up the InMobi SDK Demographic params.
        Publisher need to set the values of params as they want.

		InMobiSdk.setAreaCode("areacode");
		InMobiSdk.setEducation(Education.HIGH_SCHOOL_OR_LESS);
		InMobiSdk.setGender(Gender.MALE);
		InMobiSdk.setAge(23);
		InMobiSdk.setPostalCode("postalcode");
		InMobiSdk.setLogLevel(LogLevel.DEBUG);
		InMobiSdk.setLocationWithCityStateCountry("blore", "kar", "india");
		InMobiSdk.setLanguage("ENG");
		InMobiSdk.setInterests("dance");
		InMobiSdk.setYearOfBirth(1980);*/
        try {
            imbanner = new InMobiBanner(context, placementId);
        } catch (SdkNotInitializedException e) {
            handleBannerInitializationFailure();
            return;
        }

        imbanner.setListener(new BannerAdEventListener() {
            @Override
            public void onAdLoadSucceeded(final InMobiBanner inMobiBanner) {
                super.onAdLoadSucceeded(inMobiBanner);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "InMobi banner ad loaded successfully.");
                if (mBannerListener != null) {
                    if (inMobiBanner != null) {
                        mBannerListener.onBannerLoaded(inMobiBanner);
                    } else {
                        mBannerListener.onBannerFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
                    }
                }
            }


            @Override
            public void onAdLoadFailed(final InMobiBanner inMobiBanner,
                                       final InMobiAdRequestStatus inMobiAdRequestStatus) {
                super.onAdLoadFailed(inMobiBanner, inMobiAdRequestStatus);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "Ad failed to load");

                if (mBannerListener != null) {

                    if (inMobiAdRequestStatus.getStatusCode() == StatusCode.INTERNAL_ERROR) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.INTERNAL_ERROR);
                    } else if (inMobiAdRequestStatus.getStatusCode() == StatusCode.REQUEST_INVALID) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
                    } else if (inMobiAdRequestStatus.getStatusCode() == StatusCode.NETWORK_UNREACHABLE) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.NETWORK_INVALID_STATE);
                    } else if (inMobiAdRequestStatus.getStatusCode() == StatusCode.NO_FILL) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.NO_FILL);
                    } else if (inMobiAdRequestStatus.getStatusCode() == StatusCode.REQUEST_TIMED_OUT) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.NETWORK_TIMEOUT);
                    } else if (inMobiAdRequestStatus.getStatusCode() == StatusCode.SERVER_ERROR) {
                        mBannerListener.onBannerFailed(MoPubErrorCode.SERVER_ERROR);
                    } else {
                        mBannerListener.onBannerFailed(MoPubErrorCode.UNSPECIFIED);
                    }
                }
            }


            @Override
            public void onAdClicked(final InMobiBanner inMobiBanner, final Map<Object, Object> map) {
                super.onAdClicked(inMobiBanner, map);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "Ad interaction");
                mBannerListener.onBannerClicked();
            }


            @Override
            public void onAdDisplayed(final InMobiBanner inMobiBanner) {
                super.onAdDisplayed(inMobiBanner);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "Ad displayed");
            }


            @Override
            public void onAdDismissed(final InMobiBanner inMobiBanner) {
                super.onAdDismissed(inMobiBanner);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "Ad Dismissed");
            }


            @Override
            public void onUserLeftApplication(final InMobiBanner inMobiBanner) {
                super.onUserLeftApplication(inMobiBanner);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "User left applicaton");
                mBannerListener.onLeaveApplication();
            }

            @Override
            public void onRewardsUnlocked(InMobiBanner ad, Map<Object, Object> rewards) {
                super.onRewardsUnlocked(ad, rewards);
                MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, TAG, "Ad rewarded");
            }
        });
        imbanner.setEnableAutoRefresh(false);
        imbanner.setAnimationType(AnimationType.ANIMATION_OFF);

        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(dm);
        Map<String, String> map = new HashMap<String, String>();
        map.put("tp", "c_mopub");
        map.put("tp-ver", MoPub.SDK_VERSION);
        imbanner.setExtras(map);

        imbanner.setLayoutParams(new LinearLayout.LayoutParams(Math.round(adWidth * dm.density),
                Math.round(adHeight * dm.density)));
        imbanner.load();
    }

    private void handleBannerInitializationFailure() {
        mIsInMobiSdkInitialized = false;
        mBannerListener.onBannerFailed(MoPubErrorCode.ADAPTER_CONFIGURATION_ERROR);
    }

    @Override
    protected void onInvalidate() {
        if (imbanner != null) {
            imbanner.destroy();
        }
    }


    private class AdSize {
        private int mWidth;
        private int mHeight;


        public AdSize(int var1, int var2) {
            mWidth = var1;
            mHeight = var2;
        }


        public int getHeight() {
            return mHeight;
        }


        public int getWidth() {
            return mWidth;
        }
    }
}

