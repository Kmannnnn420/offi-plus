package de.schildbach.oeffi.ads;

import android.app.Activity;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mopub.mobileads.MoPubErrorCode;

import de.schildbach.oeffi.BuildConfig;

public class MoPubInterstitial {

    private com.mopub.mobileads.MoPubInterstitial moPubInterstitial;
    private Activity mActivity;
    private MutableLiveData<Boolean> isInterstitialReady = new MutableLiveData<>();

    public MoPubInterstitial(Activity activity) {
        this.mActivity = activity;
        moPubInterstitial = new com.mopub.mobileads.MoPubInterstitial(mActivity, BuildConfig.DEBUG ? "24534e1901884e398f1253216226017e" : "362da1882dfc49f3a33b92ea1708f3a5");
        //moPubInterstitial = new com.mopub.mobileads.MoPubInterstitial(mActivity, "362da1882dfc49f3a33b92ea1708f3a5");
        moPubInterstitial.setKeywords("Navigation, Maps, Bus, Bahn, Flixbus, Netzpläne, Haltestellen");
        moPubInterstitial.setInterstitialAdListener(new com.mopub.mobileads.MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(com.mopub.mobileads.MoPubInterstitial interstitial) {
                isInterstitialReady.setValue(true);
                Log.d(this.getClass().toString(), "onAdLoaded.");

            }

            @Override
            public void onInterstitialFailed(com.mopub.mobileads.MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                isInterstitialReady.setValue(false);
                Log.d(this.getClass().toString(), "onAdFailedToLoad. With error "+errorCode.toString());

            }

            @Override
            public void onInterstitialShown(com.mopub.mobileads.MoPubInterstitial interstitial) {
                isInterstitialReady.setValue(false);
            }

            @Override
            public void onInterstitialClicked(com.mopub.mobileads.MoPubInterstitial interstitial) {
                isInterstitialReady.setValue(false);
            }

            @Override
            public void onInterstitialDismissed(com.mopub.mobileads.MoPubInterstitial interstitial) {
                isInterstitialReady.setValue(false); }
        });

        loadMoPubView();
    }

    private void loadMoPubView() {
        if(MoPubSdk.eligibleForAds(MoPubSdk.AdType.INTERSTITIAL)) {
            moPubInterstitial.load();
            Log.i("MoPubInterstitial", "Werbung wird gezeigt!");
        }
        else {
            Log.i("MoPubInterstitial", "Werbung wird nicht gezeigt!");
        }
    }

    public com.mopub.mobileads.MoPubInterstitial getMoPubInterstitial() {
        return moPubInterstitial;
    }

    public MutableLiveData<Boolean> getIsInterstitialReady() {
        return isInterstitialReady;
    }
}


