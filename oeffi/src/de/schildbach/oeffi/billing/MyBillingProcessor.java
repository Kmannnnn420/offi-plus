package de.schildbach.oeffi.billing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.OnLifecycleEvent;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

/**
 * Created by Sönke Gissel on 02.06.2019.
 */
public class MyBillingProcessor implements LifecycleObserver, BillingProcessor.IBillingHandler{

    public enum BillingProcessorResult {
        PLAYSERVICES_NOT_AVAILABLE,
        ALREADY_PURCHASED,
        JUST_PURCHASED,
        READY_TO_PURCHASE
    }

    private BillingProcessor billingProcessor;

    private final MutableLiveData<BillingProcessorResult> billingProcessorResultMutableLiveData = new MutableLiveData<>();

    @SuppressLint("StaticFieldLeak")
    private static MyBillingProcessor INSTANCE;
    private Context mContext;

    public static MyBillingProcessor with(Context mContext) {
        if(MyBillingProcessor.INSTANCE == null) {
            MyBillingProcessor.INSTANCE = new MyBillingProcessor(mContext);
        }
        return MyBillingProcessor.INSTANCE;
    }

    private MyBillingProcessor(Context context) {
        this.mContext = context;
        billingProcessor = BillingProcessor.newBillingProcessor(mContext,
                Encryption.decrypt(BillingConstants.GOOGLE_APIKEY,
                        BillingConstants.GOOGLE_APIKEY_SALT),
                this);
    }

    public void initialize() {
        Log.i("MyBillingProcessor", "initialize. isIabServiceAvailable: "+BillingProcessor.isIabServiceAvailable(mContext));
        if(BillingProcessor.isIabServiceAvailable(mContext)) {
            billingProcessor.initialize();
        } else {
            //TODO 2 Nachricht an Firebase über Vorhandensein von Google Play
            billingProcessorResultMutableLiveData.setValue(BillingProcessorResult.PLAYSERVICES_NOT_AVAILABLE);
        }
    }

//TODO
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void releaseInstance() {
        if(billingProcessor == null) {
            throw new NullPointerException("BillingProcessor not initialized.");
        }
        Log.d("MyBillingProcessor", "releaseInstance.");
        billingProcessor.release();
    }

    public BillingProcessor getBillingProcessor() {
        Log.d("MyBillingProcessor", "returning BillingProcessor.");
        return billingProcessor;
    }

    public MutableLiveData<BillingProcessorResult> getBillingProcessorResult() {
        return billingProcessorResultMutableLiveData;
    }

    @Override
    public void onBillingInitialized() {
        Log.d("MyBillingProcessor", "onBillingInitialized.");
        /*
         * Called when BillingProcessor was initialized and it's ready to purchase
         */
        //TODO 3 Von Zeit zu Zeit den aktuellen Status der Käufe bei Google nachfragen.
        billingProcessor.loadOwnedPurchasesFromGoogle();

        Log.d("MyBillingProcessor", String.format("%s is%s purchased", BillingConstants.BILLING_PRODUCTS_ADFREE, billingProcessor.isPurchased(BillingConstants.BILLING_PRODUCTS_ADFREE) ? "" : " not"));
        if(billingProcessor.isPurchased(BillingConstants.BILLING_PRODUCTS_ADFREE)) {
            billingProcessorResultMutableLiveData.setValue(BillingProcessorResult.ALREADY_PURCHASED);
        } else {
            billingProcessorResultMutableLiveData.setValue(BillingProcessorResult.READY_TO_PURCHASE);
        }
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        //Wird wirklich direkt nach dem Kauf aufgerufen. Könnte eine Snackbar einblenden.
        Log.i("MyBillingProcessor", "onProductPurchased. Just purchased this product: "+productId);
        if(details != null) {
            Log.i("MyBillingProcessor", "onProductPurchased. More details: "+details.purchaseInfo.responseData);
        }
        if(productId.equals(BillingConstants.BILLING_PRODUCTS_ADFREE))
            billingProcessorResultMutableLiveData.setValue(BillingProcessorResult.JUST_PURCHASED);
    }

    @Override
    public void onPurchaseHistoryRestored() {
        /*
         * Called when purchase history was restored and the list of all owned PRODUCT ID's
         * was loaded from Google Play
         */
        Log.i("MyBillingProcessor", "onPurchaseHistoryRestored.");
    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        /*
         * Called when some error occurred. See Constants class for more details
         *
         * Note - this includes handling the case where the user canceled the buy dialog:
         * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
         */
        Log.e("MyBillingProcessor", "onBillingError. errorCode "+errorCode+". With error "+error);
    }
}


